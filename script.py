import shutil
import tempfile

FILE = r"C:\Users\User\PycharmProjects\loggerparser\test.py"  # Full path to file to parse
LOGGER_ID = "logger" + "."  # if "." is not included, this also affects the definition of logger, as in logger = logging.getLogger...
DESCRIPTION = ""  # Description string to use inside <editor-fold desc="">, leave empty to only see ... in PyCharm
SAFE_MODE = True  # If set to True, this will make a backup of the target file in the temp directory.


def find_multiline_offset(lines, index):
    original_index = index
    while True:  # use while True instead of directly checking for str[-1] as "" would raise IndexError
        if lines[index].split("#")[0].rstrip() == "":
            index += 1
            continue
        if lines[index].split("#")[0].rstrip()[-1] != ")":
            index += 1
            continue
        break
    return index - original_index


with open(FILE, "r") as file:
    lines = file.readlines()
    if SAFE_MODE:
        tempdir = tempfile.mkdtemp()
        shutil.copy(FILE, tempdir)
    skip_next_line = False
    for index, line in enumerate(lines):
        if skip_next_line:
            skip_next_line = False
            continue
        if line.lstrip()[:len(LOGGER_ID)] == LOGGER_ID:
            tabbing = line.replace(line.lstrip(), "")
            if index != len(lines) - 1:
                lines.insert(index, f'{tabbing}# <editor-fold desc="{DESCRIPTION}">\n')
                accounted_index = index + 1  # account for the line we just added that didn't enumerate yet
                multiline_offset = find_multiline_offset(lines, accounted_index)
                lines.insert(accounted_index + 1 + multiline_offset, f'{tabbing}# </editor-fold>\n')
                skip_next_line = True
            elif index == len(lines) - 1:
                lines.insert(index, f'{tabbing}# <editor-fold desc="{DESCRIPTION}">\n')
                lines.append(f'{tabbing}\n# </editor-fold>\n')
                break
with open(FILE, "w") as file:
    file.write("".join(lines))
